json.extract! task, :id, :description, :color, :status, :created_at, :updated_at
json.url task_url(task, format: :json)