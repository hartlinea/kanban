class Task < ApplicationRecord
  has_many :task_histories
  belongs_to :color

  enum status: [:not_started, :in_progress, :completed, :archived]

  # alias class method through so editors can stop being mad about "method not exist"
  # noinspection RubyResolve
  class <<self
    # alias colors colors
    alias statuses statuses
  end

  validates :status, inclusion: {in: statuses.keys}

  before_validation :set_default_status

  after_create :log_hist
  after_update :log_hist

  def set_default_status
    if self.status.nil?
      self.status = :not_started
    end
  end

  def log_hist
    TaskHistory.create!(description: description, color_id: color_id, status: status, task: self)
  end

  # region actions {{{
  def start
    if status.to_sym.eql? :not_started
      update!(status: :in_progress)
    else
      false
    end
  end

  def complete
    if status.to_sym.eql? :in_progress
      update!(status: :completed)
    else
      false
    end
  end

  def unstart
    if status.to_sym.eql? :in_progress
      update!(status: :not_started)
    else
      false
    end
  end

  def uncomplete
    if status.to_sym.eql? :completed
      update!(status: :in_progress)
    else
      false
    end
  end

  def archive
    update!(status: :archived)
  end
  # }}} endregion actions
end
