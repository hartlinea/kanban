class Color < ApplicationRecord
  has_many :tasks

  def to_s
    "#{name} - \##{primary}"
  end
end
