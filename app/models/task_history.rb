class TaskHistory < ApplicationRecord
  belongs_to :task

  enum status: [:not_started, :in_progress, :completed, :archived]

  # alias class method through so editors can stop being mad about "method not exist"
  # noinspection RubyResolve
  class <<self
    alias statuses statuses
  end

  validates :status, inclusion: {in: statuses.keys}


end
