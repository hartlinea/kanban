class TaskHistoriesController < ApplicationController

  def index
    @task_histories = TaskHistory.all.order(:created_at).reverse
  end

  def magic
    # ActiveRecordModel.joins - how do you work?
    history_groups = TaskHistory.where(task: Task.where.not(status: :archived)).group_by(&:task)

    @time_pending_by_task = {}

    history_groups.each { |task, hist_records|
      @time_pending_by_task[task] = 0
      hist_records.sort_by { |record| record[:id] }
      last_action_was_to_in_progress = false
      last_start_progress = nil
      hist_records.each { |record|
        if record.status.to_sym.eql? :in_progress
          unless last_action_was_to_in_progress
            last_action_was_to_in_progress = true
            last_start_progress = record.created_at
          end
        else
          if last_action_was_to_in_progress
            @time_pending_by_task[task] += (record.created_at - last_start_progress)
          end
          last_action_was_to_in_progress = false
        end
      }
      if last_action_was_to_in_progress
        @time_pending_by_task[task] += Time.now - last_start_progress
      end
    }

    @time_pending_by_task = Hash[@time_pending_by_task.sort_by { |_, v| v }.reverse]
  end
end
