require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cook_dinner = tasks(:cook_dinner)
    @finish_lab = tasks(:finish_lab)
  end

  test 'should create task and subsequent task history' do
    assert_difference('Task.count') do
      assert_difference('TaskHistory.count') do
        post tasks_url, params: {task: {color_id: @cook_dinner.color.id, description: @cook_dinner.description, status: @cook_dinner.status}}
      end
    end

    assert_redirected_to tasks_url
    assert Task.statuses[:in_progress], TaskHistory.last.status
  end

  test 'start action should start task and create history' do
    assert_difference('TaskHistory.count') do
      get start_task_url(@cook_dinner), params: {}
    end

    assert_redirected_to tasks_url
    assert Task.statuses[:in_progress], @cook_dinner.status
    assert Task.statuses[:in_progress], TaskHistory.last.status
  end

  test 'complete action should fail on not started task' do
    assert_difference('Task.count') do
      assert_difference('TaskHistory.count') do
        post tasks_url, params: {task: {color_id: @cook_dinner.color.id, description: @cook_dinner.description, status: @cook_dinner.status}}
      end
    end
    assert_no_difference('TaskHistory.count') do
      get complete_task_url(@cook_dinner), params: {}
    end

    assert Task.statuses[:not_started], @cook_dinner.status
    assert Task.statuses[:not_started], TaskHistory.last.status
  end

  test 'archive archives a task' do
    assert_difference('TaskHistory.count') do
      get archive_task_url(@finish_lab), params: {}
    end

    assert Task.statuses[:archived], @finish_lab.status
    assert Task.statuses[:archived], TaskHistory.last.status
  end
end
