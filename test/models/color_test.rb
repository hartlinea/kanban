require 'test_helper'

class ColorTest < ActiveSupport::TestCase
  setup do
    @red = colors(:red)
    @blue = colors(:blue)
  end

  test 'to_s method' do
    assert 'red - #cc241d', @red.to_s
  end
end
