class CreateColors < ActiveRecord::Migration[5.0]
  def change
    create_table :colors do |t|
      t.string :name
      t.string :primary
      t.string :secondary

      t.timestamps
    end
  end
end
