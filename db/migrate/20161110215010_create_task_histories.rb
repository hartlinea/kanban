class CreateTaskHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :task_histories do |t|
      t.string :description
      t.integer :status
      t.references :color, index: true, foreign_key: true
      t.references :task, index: true, foreign_key: true

      t.timestamps
    end
  end
end
