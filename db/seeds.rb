
# colors from gruvbox - https://github.com/morhetz/gruvbox
COLOR_MAPPING = {
    dark: '282828',
    red: 'cc241d',
    green: '98971a',
    yellow: 'd79921',
    blue: '458588',
    purple: 'b16288',
    aqua: '689d6a',
    gray: 'a89984'
}
COLOR_MAPPING_ACCENT = {
    dark: '928374',
    red: 'fb4934',
    green: 'b8bb26',
    yellow: 'fabd2f',
    blue: '831598',
    purple: 'd3869b',
    aqua: '8ec07c',
    gray: 'ebdbb2'
}

COLOR_MAPPING.each_key do |key|
  Color.create!(name: key.to_s, primary: COLOR_MAPPING[key], secondary: COLOR_MAPPING_ACCENT[key])
end