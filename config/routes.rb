Rails.application.routes.draw do
  resources :tasks do
    member do
      get 'start', as: :start
      get 'complete', as: :complete
      get 'uncomplete', as: :uncomplete
      get 'unstart', as: :unstart
      get 'archive', as: :archive
    end
  end
  resources :colors

  get '/taskhistory' => 'task_histories#index', as: :task_history
  get '/magic' => 'task_histories#magic', as: :magic

end
